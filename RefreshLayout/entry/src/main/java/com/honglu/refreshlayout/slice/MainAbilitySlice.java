package com.honglu.refreshlayout.slice;

import com.honglu.refreshlayout.RefreshLayout;
import com.honglu.refreshlayout.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ListContainer;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainAbilitySlice extends AbilitySlice implements RefreshLayout.OnRefreshListener{

    private RefreshLayout refreshLayout;
    private ListProvider mListProvider;

    private final EventHandler eventHandler = new EventHandler(EventRunner.getMainEventRunner());

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        refreshLayout = (RefreshLayout) findComponentById(ResourceTable.Id_refreshLayout);
        refreshLayout.setOnRefreshListener(this);
        ListContainer listContainer = (ListContainer) findComponentById(ResourceTable.Id_listContainer);
        mListProvider = new ListProvider(this);
        listContainer.setItemProvider(mListProvider);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void loading() {
        eventHandler.postTask(()->{
            refreshLayout.loadComplete();
            mListProvider.addList(createItems(10));
        },1000);

    }

    @Override
    public void refreshing() {
        eventHandler.postTask(()->{
            mListProvider.setList(createItems(20));
            refreshLayout.refreshComplete();
        },3000);
    }

    private List<String> createItems(int count){
        List<String> temp = new ArrayList<>();
        for(int i=0;i<count;i++){
            temp.add("this is item " + i);
        }
        return temp;
    }
}
