/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.honglu.refreshlayout.slice;

import com.honglu.refreshlayout.ResourceTable;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

public class ListProvider extends BaseItemProvider {

    private List<String> mList = new ArrayList<>();
    private Context context;
    private LayoutScatter inflater;

    public ListProvider(Context context) {
        this.context = context;
        this.inflater = LayoutScatter.getInstance(context);
    }

    public void setList(List<String> data){
        mList.clear();
        if(data != null && !data.isEmpty()){
            mList.addAll(data);
        }
        notifyDataChanged();
    }

    public void addList(List<String> data){
        if(data != null && !data.isEmpty()){
            mList.addAll(data);
            notifyDataSetItemRangeChanged(mList.size() -1 ,data.size());
        }
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public String getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component convertView, ComponentContainer componentContainer) {
        Component view = convertView;
        final ViewHolder holder;
        if (convertView == null) {
            view = inflater.parse(ResourceTable.Layout_item_list, componentContainer, false);
            holder = new ViewHolder();
            holder.itemText = (Text) view.findComponentById(ResourceTable.Id_item_text);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.itemText.setText(getItem(i));
        return view;
    }

    static class ViewHolder {
        Text itemText;
    }

}
