package com.honglu.refreshlayout.slice;

import com.honglu.refreshlayout.RefreshLayout;
import com.honglu.refreshlayout.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

public class TextAbilitySlice extends AbilitySlice implements RefreshLayout.OnRefreshListener {

    private RefreshLayout refreshLayout;
    private final EventHandler eventHandler = new EventHandler(EventRunner.getMainEventRunner());

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_text);
        refreshLayout = (RefreshLayout) findComponentById(ResourceTable.Id_refreshLayout);
        refreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void loading() {
        eventHandler.postTask(()->refreshLayout.loadComplete(),1000);
    }

    @Override
    public void refreshing() {
        eventHandler.postTask(()->refreshLayout.refreshComplete(),1000);
    }
}
