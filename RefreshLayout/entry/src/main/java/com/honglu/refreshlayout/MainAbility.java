package com.honglu.refreshlayout;

import com.honglu.refreshlayout.slice.MainAbilitySlice;
import com.honglu.refreshlayout.slice.TextAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
//        super.setMainRoute(MainAbilitySlice.class.getName());
        super.setMainRoute(TextAbilitySlice.class.getName());
    }
}
