package com.honglu.refreshlayout;


import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.app.Context;

import static ohos.agp.components.ComponentContainer.LayoutConfig.MATCH_PARENT;
import static ohos.agp.utils.LayoutAlignment.BOTTOM;
import static ohos.agp.utils.LayoutAlignment.TOP;

/**
 * create by ly
 */
public class RefreshLayout extends StackLayout implements Component.DraggedListener {


    private Context mContext;

    private float mPullHeight;
    private float mHeaderHeight;
    private Component mContentView;
    private Component footer;
    private Component header;


    private static final String EnableLoadMore = "EnableLoadMore";
    private static final String EnableRefresh = "EnableRefresh";
    private static final String RefreshBg = "RefreshBg";
    private static final String RotateIconId = "RotateIconId";

    private boolean enableLoadMore = false;
    private boolean enableRefresh = false;
    private int refreshBgColor;
    private Element rotateIcon = null;

    private int startY;
    private Image refreshImage;
    private Image loadImage;

    public RefreshLayout(Context context) {
        this(context, null, null);
    }

    public RefreshLayout(Context context, AttrSet attrs) {
        this(context, attrs, null);
    }

    public RefreshLayout(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init(context, attrs);
    }

    private void init(Context context, AttrSet attrs) {
        mContext = context;
        if (getChildCount() > 1) {
            throw new RuntimeException("you can only attach one child");
        }
        setAttrs(attrs);
        mPullHeight = 220;
        mHeaderHeight = 160;
        setLayoutRefreshedListener(component -> {
            if (header == null) {
                addHeaderView();
                addFooter();
                mContentView = getComponentAt(0);
            }
        });
        setDraggedListener(DRAG_VERTICAL, this);
    }

    private void setAttrs(AttrSet attrs) {

        boolean enableLoadMore = attrs.getAttr(EnableLoadMore).isPresent();
        if (enableLoadMore) {
            this.enableLoadMore = attrs.getAttr(EnableLoadMore).get().getBoolValue();
        }
        boolean enableRefresh = attrs.getAttr(EnableRefresh).isPresent();
        if (enableRefresh) {
            this.enableRefresh = attrs.getAttr(EnableRefresh).get().getBoolValue();
        }
        boolean refreshBg = attrs.getAttr(RefreshBg).isPresent();
        if (refreshBg) {
            this.refreshBgColor = attrs.getAttr(RefreshBg).get().getColorValue().getValue();
        }else{
            this.refreshBgColor = Color.getIntColor("#22000000");
        }
        boolean rotateIconId = attrs.getAttr(RotateIconId).isPresent();
        if (rotateIconId) {
            rotateIcon = attrs.getAttr(RotateIconId).get().getElement();
        }
    }

    private boolean isRefresh = false;
    private boolean isLoad = false;

    private void onDrugUpdate(DragInfo dragInfo) {
        int upY = Math.round(dragInfo.updatePoint.position[1]);
        float dy = upY - startY;
        if (Math.abs(dy) > 20) {
            if (!Constant.IS_REFRESH_ING) {
                if (dy > 0 && enableRefresh) {
                    isRefresh = true;
                    isLoad = false;
                    dy = Math.min(mPullHeight * 2, dy);
                    dy = Math.max(0, dy);
                    translateHeader(dy);
                } else if (dy < 0 && enableLoadMore) {
                    dy = Math.abs(dy);
                    dy = Math.min(mPullHeight * 2, dy);
                    dy = Math.max(0, dy);
                    isRefresh = false;
                    isLoad = true;
                    translateFooter(dy);
                }
            }
        }
    }

    private void onDrugEnd(DragInfo dragInfo) {
        Point mPoint = dragInfo.updatePoint;
        int endY = Math.round(mPoint.position[1]);
        int dy = Math.abs(endY - startY);
        if (dy > mPullHeight * 2) {
            if (onCircleRefreshListener != null) {
                if (isRefresh && enableRefresh) {
                    startRefresh();
                    onCircleRefreshListener.refreshing();
                } else if (isLoad && enableLoadMore) {
                    startLoad();
                    onCircleRefreshListener.loading();
                }
            }
        } else {
            if(endY > startY) {
                refreshComplete();
            }else {
                loadComplete();
            }
        }
    }

    private void startRefresh() {
        LayoutConfig layoutConfig =
                (LayoutConfig) header.getLayoutConfig();
        layoutConfig.height = (int) mHeaderHeight;
        header.setLayoutConfig(layoutConfig);
        header.invalidate();
        mContentView.setTranslationY(mHeaderHeight);

        refreshAnim.start();
        Constant.IS_REFRESH_ING = true;
        Constant.IS_FINISH_REFRESH = false;
    }

    private void startLoad() {
        LayoutConfig layoutConfig =
                (LayoutConfig) footer.getLayoutConfig();
        layoutConfig.height = (int) mHeaderHeight;
        footer.setLayoutConfig(layoutConfig);
        footer.invalidate();
        mContentView.setTranslationY(-mHeaderHeight);

        loadAnim.start();
        Constant.IS_REFRESH_ING = true;
        Constant.IS_FINISH_REFRESH = false;
    }

    public void refreshComplete() {
        LogUtil.error("-----refreshComplete-----");
        Constant.IS_REFRESH_ING = false;
        Constant.IS_FINISH_REFRESH = true;
        refreshAnim.stop();

        isRefresh = false;
        isLoad = false;

        refreshClose.start();
    }

    public void loadComplete() {
        LogUtil.error("-----loadComplete-----");
        Constant.IS_REFRESH_ING = false;
        Constant.IS_FINISH_REFRESH = true;
        loadAnim.stop();

        isRefresh = false;
        isLoad = false;

        loadClose.start();
    }

    private void addHeaderView() {
        LogUtil.error("--------addHeaderView-----------");
        header = LayoutScatter.getInstance(mContext)
                .parse(ResourceTable.Layout_refresh, null, false);
        if(refreshBgColor!=0) {
            ShapeElement element = new ShapeElement();
            element.setRgbColor(RgbColor.fromArgbInt(refreshBgColor));
            header.setBackground(element);
        }
        refreshImage = (Image) header.findComponentById(ResourceTable.Id_refresh);
        if(rotateIcon != null) {
            refreshImage.setImageElement(rotateIcon);
        }else{
            refreshImage.setImageAndDecodeBounds(ResourceTable.Media_refresh);
        }
        LayoutConfig config = new LayoutConfig();
        config.width = MATCH_PARENT;
        config.height = 0;
        config.alignment = TOP;
        super.addComponent(header, 1, config);
        initHeadAmin();
    }

    private void addFooter() {
        LogUtil.error("--------addBottom-----------");
        footer = LayoutScatter.getInstance(mContext)
                .parse(ResourceTable.Layout_load_more, null, false);

        if(refreshBgColor!=0) {
            ShapeElement element = new ShapeElement();
            element.setRgbColor(RgbColor.fromArgbInt(refreshBgColor));
            footer.setBackground(element);
        }
        loadImage = (Image) footer.findComponentById(ResourceTable.Id_load);
        if(rotateIcon != null) {
            loadImage.setImageElement(rotateIcon);
        }else{
            loadImage.setImageAndDecodeBounds(ResourceTable.Media_refresh);
        }
        LayoutConfig config = new LayoutConfig();
        config.alignment = BOTTOM;
        config.width = MATCH_PARENT;
        config.height = 0;
        super.addComponent(footer, 2, config);
        initLoadAmin();
    }

    private AnimatorValue refreshAnim;
    private AnimatorValue loadAnim;

    private AnimatorValue refreshClose;
    private AnimatorValue loadClose;

    private void initHeadAmin() {
        refreshAnim = new AnimatorValue();
        refreshAnim.setDuration(1000);
        refreshAnim.setLoopedCount(Animator.INFINITE);
        refreshAnim.setCurveType(Animator.CurveType.LINEAR);
        refreshAnim.setValueUpdateListener((animatorValue, v) -> {
            refreshImage.setRotation(v * 360);
        });

        refreshClose = new AnimatorValue();
        refreshClose.setDuration(500);
        refreshClose.setLoopedCount(0);
        refreshClose.setCurveType(Animator.CurveType.LINEAR);
        refreshClose.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
//                refreshImage.setRotation(v * 360);
                LayoutConfig layoutConfig =
                        (LayoutConfig) header.getLayoutConfig();
                int height = layoutConfig.height;
                float rate = 1.0f - v;
                if (rate < 0.1) {
                    rate = 0;
                    refreshClose.stop();
                }
                int rateHeight = (int) (height * rate);
                layoutConfig.height = rateHeight;
                header.setLayoutConfig(layoutConfig);
                header.invalidate();
                mContentView.setTranslationY(rateHeight);
            }


        });

    }

    private void initLoadAmin() {
        loadAnim = new AnimatorValue();
        loadAnim.setDuration(1000);
        loadAnim.setLoopedCount(Animator.INFINITE);
        loadAnim.setCurveType(Animator.CurveType.LINEAR);
        loadAnim.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                loadImage.setRotation(v * 360);
            }
        });

        loadClose = new AnimatorValue();
        loadClose.setDuration(500);
        loadClose.setLoopedCount(0);
        loadClose.setCurveType(Animator.CurveType.LINEAR);
        loadClose.setValueUpdateListener((animatorValue, v) -> {
//                refreshImage.setRotation(v * 360);
            LayoutConfig layoutConfig =
                    (LayoutConfig) footer.getLayoutConfig();
            int height = layoutConfig.height;
            float rate = 1.0f - v;
            if (rate < 0.1) {
                rate = 0;
                loadClose.stop();
            }
            int rateHeight = (int) (height * rate);
            layoutConfig.height = rateHeight;
            footer.setLayoutConfig(layoutConfig);
            footer.invalidate();
            mContentView.setTranslationY(-rateHeight);
        });
    }

    private void translateHeader(float dy) {
        float offsetY = (dy / 2 / mPullHeight) * dy / 2;

        float rate = offsetY / mPullHeight;

        refreshImage.setRotation(rate * 360);

        LayoutConfig layoutConfig =
                (LayoutConfig) header.getLayoutConfig();
        layoutConfig.height = (int) offsetY;

//        LogUtil.error("-------translateHeader------------>  " + offsetY);
//        header.setHeight((int) offsetY);
        header.setLayoutConfig(layoutConfig);
        header.invalidate();

        mContentView.setTranslationY(offsetY);
    }

    private void translateFooter(float dy) {

//        setTranslationY((dy / 2 / mPullHeight) * dy / 2);
        float offsetY = (dy / 2 / mPullHeight) * dy / 2;


        float rate = offsetY / mPullHeight;

        loadImage.setRotation(rate * 360);

//        LogUtil.error("-------translateFooter-------->  " + offsetY);
        LayoutConfig layoutConfig =
                (LayoutConfig) footer.getLayoutConfig();
        layoutConfig.height = (int) offsetY;
//        header.setHeight((int) offsetY);
        footer.setLayoutConfig(layoutConfig);
        footer.invalidate();

        mContentView.setTranslationY(-offsetY);
    }

    private OnRefreshListener onCircleRefreshListener;

    public void setOnRefreshListener(OnRefreshListener onCircleRefreshListener) {
        this.onCircleRefreshListener = onCircleRefreshListener;
    }

    @Override
    public void onDragDown(Component component, DragInfo dragInfo) {
        LogUtil.error("-----onDragDown-----");
        startY = Math.round(dragInfo.downPoint.position[1]);
    }

    @Override
    public void onDragStart(Component component, DragInfo dragInfo) {
        LogUtil.error("-----onDragStart-----");
    }

    @Override
    public void onDragUpdate(Component component, DragInfo dragInfo) {
        onDrugUpdate(dragInfo);
    }

    @Override
    public void onDragEnd(Component component, DragInfo dragInfo) {
        onDrugEnd(dragInfo);
    }

    @Override
    public void onDragCancel(Component component, DragInfo dragInfo) {
        LogUtil.error("-----onDragCancel-----");
    }

    public interface OnRefreshListener {
        void loading();

        void refreshing();
    }


    public void setRefreshEnabled(boolean enabled){
        this.enableRefresh = enabled;
    }

    public void setLoadMoreEnabled(boolean enabled){
        this.enableLoadMore = enabled;
    }

}
