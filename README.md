# harmonyos-refreshlayout

#### 介绍
基于harmonyos的下拉刷新及上拉加载的容器组件

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

```
<com.honglu.refreshlayout.RefreshLayout
        ohos:id="$+id:refreshLayout"
        ohos:height="match_parent"
        app:EnableLoadMore="true"
        app:EnableRefresh="true"
        app:RefreshBg="$color:refresh_bg"
        app:RotateIconId="$media:icon"
        ohos:width="match_parent">
            <ListContainer
                ohos:id="$+id:listContainer"
                ohos:height="match_parent"
                ohos:width="match_parent"/>

</com.honglu.refreshlayout.RefreshLayout>
```

```
<com.honglu.refreshlayout.RefreshLayout
        ohos:id="$+id:refreshLayout"
        ohos:height="match_parent"
        ohos:width="match_parent"
        app:EnableLoadMore="true"
        app:EnableRefresh="true">

        <Text
            ohos:id="$+id:text"
            ohos:text_size="16fp"
            ohos:multiple_lines="true"
            ohos:text_alignment="top"
            ohos:height="match_parent"
            ohos:width="match_parent"/>

    </com.honglu.refreshlayout.RefreshLayout>
```


```
import com.honglu.refreshlayout.RefreshLayout;
import com.honglu.refreshlayout.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

public class TextAbilitySlice extends AbilitySlice implements RefreshLayout.OnRefreshListener {

    private RefreshLayout refreshLayout;
    private final EventHandler eventHandler = new EventHandler(EventRunner.getMainEventRunner());

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_text);
        refreshLayout = (RefreshLayout) findComponentById(ResourceTable.Id_refreshLayout);
        refreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void loading() {
        eventHandler.postTask(()->refreshLayout.loadComplete(),1000);
    }

    @Override
    public void refreshing() {
        eventHandler.postTask(()->refreshLayout.refreshComplete(),1000);
    }
}

```


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
